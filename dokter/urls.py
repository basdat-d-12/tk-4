from django.conf.urls import url
from django.urls import include,path
from . import views


app_name='dokter'

urlpatterns = [
    path('assign-dokter/',views.assign,name='assign-dokter'),
    path('display/',views.display_dokter_rs_cabang,name='display-dokter'),
    path('delete/',views.delete_dokter_rscabang,name='delete-dokter'),
    path('update/',views.update_dokter,name='update-dokter'),
    path('delete-view/',views.display_dokter_rs_cabang_delete,name='delete-view'),
    path('update-view/',views.display_dokter_rs_cabang_update,name='update-view'),
    path('assign-fail/',views.assign_fail,name='assign-fail'),
]