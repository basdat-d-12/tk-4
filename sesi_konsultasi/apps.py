from django.apps import AppConfig


class SesiKonsultasiConfig(AppConfig):
    name = 'sesi_konsultasi'
