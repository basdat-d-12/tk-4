from django.shortcuts import render,redirect, HttpResponse
from django.db import connection
from .forms import SesiKonsultasiForm, UpdateSesiKonsultasiForm
import json
import time
from django.views.decorators.csrf import csrf_exempt

def create_sesi_konsultasi(request):
    if "is_admin" not in request.session:
        return render(request,'unauthorized.html')
    response = {}
    response['error'] = False
    id_poliklinik   = get_sesi_konsultasi()
    no_rekam_medis  = get_no_rekam_medis()
    id_transaksi    = get_id_transaksi() 
    form = SesiKonsultasiForm(request.POST or None)
    form.fields['no_rekam_medis'].choices = no_rekam_medis
    form.fields['id_transaksi'].choices = id_transaksi
    response['form'] = form

    if(request.method=="POST" and form.is_valid()):
        no_rekam_medis      = form.cleaned_data['no_rekam_medis']
        tanggal             = form.cleaned_data['tanggal']
        id_transaksi        = form.cleaned_data['id_transaksi']
        id_konsultasi       = get_new_id_konsultasi()
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into sesi_konsultasi (no_rekam_medis_pasien,tanggal,id_transaksi,id_konsultasi,biaya,status) values(%s,%s,%s,%s,%s,%s);",[no_rekam_medis,tanggal,id_transaksi,id_konsultasi,0,"BOOKED"])
        return redirect('/sesikonsultasi/display')
    
    return render(request,'form_sesi_konsultasi.html',response)

def get_new_id_konsultasi():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_konsultasi from sesi_konsultasi;")
    response = fetch(cursor)
    arr_id_konsultasi = []
    for id_konsultasi in response:
        arr_id_konsultasi.append(id_konsultasi['id_konsultasi'])
    print(max(arr_id_konsultasi))
    return int(max(arr_id_konsultasi)) + 1
    
def get_no_rekam_medis():
    query="""
    select no_rekam_medis from pasien;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    no_rekam_medis_response = fetch(cursor)
    res=[]
    for no_rekam_medis in no_rekam_medis_response:
        temp = no_rekam_medis['no_rekam_medis']
        res.append((temp,temp,))
    return res

def get_id_transaksi():
    query="""
    select id_transaksi from transaksi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_transaksi_response = fetch(cursor)
    res=[]
    for id_transaksi in id_transaksi_response:
        temp = id_transaksi['id_transaksi']
        res.append((temp,temp,))
    return res

def get_sesi_konsultasi():
    query="""
    SELECT * from sesi_konsultasi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    sesi_konsultasi_response = fetch(cursor)
    return sesi_konsultasi_response

def get_id_konsultasi(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from sesi_konsultasi where id_konsultasi=%s",[id_konsultasi])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def display_sesi_konsultasi(request):
    if "username" not in request.session:
        return redirect('/login')
    sesi_konsultasi_raw = get_sesi_konsultasi()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = sesi_konsultasi_raw
    return render(request,'daftar_sesi_konsultasi.html',response)

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_sk(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_konsultasi = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from sesi_konsultasi where id_konsultasi =%s;",[id_konsultasi])
        return redirect("/sesikonsultasi/display")
    return HttpResponse("Nothing here")

def get_no_rekam_medis_by_id(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select no_rekam_medis_pasien from sesi_konsultasi where id_konsultasi=%s",[id_konsultasi])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_biaya_by_id(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select biaya from sesi_konsultasi where id_konsultasi=%s",[id_konsultasi])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_id_transaksi_by_id(id_konsultasi):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_transaksi from sesi_konsultasi where id_konsultasi=%s",[id_konsultasi])
    res_dirty = fetch(cursor)
    return res_dirty[0]

@csrf_exempt
def update_sk(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    id_sesi_konsultasi = request.GET.get('id_konsultasi',None)
    no_medis        = get_no_rekam_medis_by_id(id_sesi_konsultasi)
    print(no_medis)
    response={}
    if(id_sesi_konsultasi != None):
        prefilled_data = get_id_konsultasi(id_sesi_konsultasi)
        response['error']=False
        form = UpdateSesiKonsultasiForm(request.POST or None, initial={'id_konsultasi':id_sesi_konsultasi,'no_rekam_medis': prefilled_data['no_rekam_medis_pasien'],'tanggal':prefilled_data['tanggal'],'biaya':prefilled_data['biaya'],'status':prefilled_data['status'],'id_transaksi':prefilled_data['id_transaksi']})
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        id_konsultasi           = form.cleaned_data['id_konsultasi']
        tanggal                 = form.cleaned_data['tanggal']
        no_rekam_medis          = form.cleaned_data['no_rekam_medis']
        biaya                   = form.cleaned_data['biaya']
        status                  = form.cleaned_data['status']
        id_transaksi            = form.cleaned_data['id_transaksi']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update sesi_konsultasi set tanggal=%s,status=%s where id_konsultasi=%s",[tanggal,status,id_konsultasi])
        return redirect("/sesikonsultasi/display")

    return render(request,'update_sesi_konsultasi.html',response)


def create_success(request):
    if 'username' in request.session and 'is_admin' in request.session:
        return render(request,'sukses.html')
    return redirect('/unauthorized')