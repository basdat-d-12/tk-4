from django import forms

class SesiKonsultasiForm(forms.Form):
    no_rekam_medis  = forms.ChoiceField(choices=[], label='No Rekam Medis Pasien :')
    tanggal         = forms.DateField(
                        label  = 'Tanggal :',
                        widget =forms.DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'required': 'required'}),
                    )
    id_transaksi    = forms.ChoiceField(choices=[], label ='ID Transaksi :')

class UpdateSesiKonsultasiForm(forms.Form):
    id_konsultasi   = forms.CharField(label='ID Konsultasi', required=False, disabled = True)
    no_rekam_medis  = forms.CharField(required=False, disabled = True)
    tanggal         = forms.DateField(
                        label  = 'Tanggal :',
                        widget =forms.DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'required': 'required'}),
                    )
    biaya           = forms.IntegerField(label='Biaya', required=False, disabled = True)
    status          = forms.CharField(label='Status')
    id_transaksi    = forms.CharField(label ='ID Transaksi :', required=False, disabled = True)