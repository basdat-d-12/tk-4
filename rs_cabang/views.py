from django.shortcuts import render,redirect
from .forms import RsCabangForm,UpdateRSCabangForm
from django.db import connection
import json
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create_rscabang(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if "username" not in request.session:
        return redirect('/login')

    response={}
    response['error']=False
    form = RsCabangForm(request.POST or None)
    response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        kodeRs              = form.cleaned_data['kodeRs']
        nama                = form.cleaned_data['nama']
        tanggalPendirian    = form.cleaned_data['tanggalPendirian']
        jalan               = form.cleaned_data['jalan']
        nomor               = form.cleaned_data['nomor']
        kota                = form.cleaned_data['kota']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into rs_cabang (kode_rs,nama,tanggal_pendirian,jalan,nomor,kota) values(%s,%s,%s,%s,%s,%s);",[kodeRs,nama,tanggalPendirian,jalan,nomor,kota])
        return redirect('/rscabang/display')
    return render(request,'form_rs_cabang.html',response)

def display_rs(request):
    if "username" not in request.session:
        return redirect('/login')
    response={}
    response['data'] = get_rs()
    response['is_admin'] = "is_admin" in request.session
    return render(request,'list_rscabang.html',response)

def get_rs():
    query = """
    select kode_rs, nama, tanggal_pendirian, jalan, kota, nomor from rs_cabang;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    obat_response = fetch(cursor)
    return obat_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_rs(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        kode_rs = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from rs_cabang where kode_rs =%s;",[kode_rs])
        return redirect("/rscabang/display")
    return HttpResponse("Nothing here")

@csrf_exempt
def update_rs(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    kode_rscab = request.GET.get('kode_rs',None)
    response={}
    if(kode_rscab != None):
        prefilled_data = get_rs_with_kode(kode_rscab)
        response['error']=False
        form = UpdateRSCabangForm(request.POST or None, initial={'kodeRs':kode_rscab,'nama':prefilled_data['nama'],'tanggalPendirian':prefilled_data['tanggal_pendirian'],'jalan':prefilled_data['jalan'],'nomor':prefilled_data['nomor'],'kota':prefilled_data['kota']})
        form.fields['kodeRs'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        kodeRs                  = form.cleaned_data['kodeRs']
        tanggalPendirian        = form.cleaned_data['tanggalPendirian']
        nama                    = form.cleaned_data['nama']
        jalan                   = form.cleaned_data['jalan']
        nomor                   = form.cleaned_data['nomor']
        kota                    = form.cleaned_data['kota']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update rs_cabang set kode_rs=%s, nama=%s, tanggal_pendirian=%s,jalan=%s,nomor=%s,kota=%s where kode_rs=%s",[kodeRs,nama,tanggalPendirian,jalan,nomor,kota,kodeRs])
        return redirect("/rscabang/display")

    return render(request,'update_rs_cabang.html',response)

def get_rs_with_kode(kode_rs):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from rs_cabang where kode_rs=%s",[kode_rs])
    res_dirty = fetch(cursor)
    return res_dirty[0]