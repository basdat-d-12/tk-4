from django import forms

class RsCabangForm(forms.Form):
    kodeRs              = forms.CharField(label = 'kode RS')
    nama                = forms.CharField(label = 'nama')
    tanggalPendirian    = forms.DateField(label = 'Tanggal Pendirian')
    jalan               = forms.CharField(label = 'Jalan')
    nomor               = forms.IntegerField(label = 'Nomor')
    kota                = forms.CharField(label = 'Kota')

class UpdateRSCabangForm(forms.Form):
    kodeRs              = forms.CharField(label = 'kode RS')
    nama                = forms.CharField(label = 'Nama')
    tanggalPendirian    = forms.CharField(label = 'Tanggal Pendirian')
    jalan               = forms.CharField(label = 'Jalan')
    kota                = forms.CharField(label = 'Kota')
    nomor               = forms.IntegerField(label = 'Nomor')

