from django.shortcuts import render, redirect
from django.db import connection
from .forms import AssignAdminForm
# Create your views here.


def assign(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if 'username' not in request.session:
        return redirect('/login')
    response = {}
    response['error'] = False
    nomor_pegawai = get_nomor_pegawai_administrator()
    kode_rs = get_kode_rs()
    form = AssignAdminForm(request.POST or None)
    form.fields['nomor_pegawai'].choices=nomor_pegawai
    form.fields['kode_rs'].choices=kode_rs
    response['form']=form
    if(request.method == "POST" and form.is_valid()):
        response['nomor_pegawai']=request.POST['nomor_pegawai']
        response['kode_rs']=request.POST['kode_rs']
        assign_admin_to_rs(response['nomor_pegawai'],response['kode_rs'])
        return redirect('/rscabang/display/')
    else:
        response['error']=True
    return render(request,'assign.html',response)


def get_nomor_pegawai_administrator():
    # fetch semua nomor_pegawai dari admin
    query1 = """
    select nomor_pegawai from administrator;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query1)

    nomor_pegawai_admin_response = fetch(cursor)
    nomor_pegawai_admin = []
    for nomor_pegawai in nomor_pegawai_admin_response:
        np = nomor_pegawai['nomor_pegawai']
        res = (np,np,)
        nomor_pegawai_admin.append(res)
    return nomor_pegawai_admin

def get_kode_rs():
        # fetch semua kode rs
    query2 = """
    select kode_rs from rs_cabang;
    """
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query2)
    kode_rs_cabang_response = fetch(cursor)
    kode_rs_cabang = []
    for kode_rs in kode_rs_cabang_response:
        kr = kode_rs['kode_rs']
        res=(kr,kr,)
        kode_rs_cabang.append(res)
    return kode_rs_cabang


def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def assign_admin_to_rs(nomor_pegawai,kode_rs):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("update administrator set kode_rs = %s where nomor_pegawai = %s;",[kode_rs,nomor_pegawai])
