from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import CreateTransaksiForm, UpdateTransaksiForm
from datetime import date, datetime
import time
from django.views.decorators.csrf import csrf_exempt
import json

def create_transaksi(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if "username" not in request.session:
        return redirect('/login')
		
    response = {}
    response['error'] = False
    no_rekam_medis = get_no_rekam_medis()
    form = CreateTransaksiForm(request.POST or None)
    form.fields['no_rekam_medis'].choices=no_rekam_medis
    response['form'] = form
    if(request.method=="POST" and form.is_valid()):
	    print(form.cleaned_data)
	    id_transaksi = get_new_id()
	    tanggal = date.today()
	    status = 'Created'
	    total_biaya = 0
	    waktu_pembayaran = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	    no_rekam_medis = form.cleaned_data['no_rekam_medis']
	    cursor = connection.cursor()
	    cursor.execute("set search_path to medika_go;")
	    cursor.execute("insert into transaksi(id_transaksi,tanggal,status,total_biaya,waktu_pembayaran,no_rekam_medis) values(%s,%s,%s,%s,%s,%s);",[id_transaksi,tanggal,status,total_biaya,waktu_pembayaran,no_rekam_medis])
	    return redirect('/transaksi/create-success')
    return render(request,'create-transaksi.html',response)

def create_success(request):
    if 'username' in request.session and 'is_admin' in request.session:
        return render(request,'sukses-transaksi.html')
    return redirect('/unauthorized')

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_no_rekam_medis():
    query="""
    select no_rekam_medis from pasien;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    no_rekam_medis_response = fetch(cursor)
    res=[]
    for no_rekam_medis in no_rekam_medis_response:
        temp = no_rekam_medis['no_rekam_medis']
        res.append((temp,temp,))
    return res

def get_transaksi():
    query="""
    select *
	from transaksi
	order by transaksi.waktu_pembayaran asc;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    transaksi_response = fetch(cursor)
    return transaksi_response

def display_transaksi(request):
    if "username" not in request.session:
        return redirect('/login')
    transaksi_raw = get_transaksi()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = transaksi_raw
    return render(request,'display-transaksi.html',response)
	
@csrf_exempt
def delete_transaksi(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_transaksi = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from transaksi where id_transaksi=%s;",[id_transaksi])
        return redirect("/transaksi/display")
    return HttpResponse("Nothing here")
	
def get_new_id():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_transaksi from transaksi;")
    id_transaksi_response = fetch(cursor)
    current = []
    for id in id_transaksi_response:
	    if(id['id_transaksi'].isdigit()):
		    current.append(id['id_transaksi'])
    if not current:
	    current.append(750000)
    return int(max(current))+1
	
@csrf_exempt
def update_transaksi(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    id = request.GET.get('id',None)
    if(id != None):
        prefilled_data = get_transaksi_with_id(id)
        response={}
        response['error']=False
        form = UpdateTransaksiForm(request.POST or None, initial={'id_transaksi':id,'tanggal':prefilled_data['tanggal'],'status':prefilled_data['status'],'total_biaya':prefilled_data['total_biaya'],'waktu_pembayaran':prefilled_data['waktu_pembayaran'],'no_rekam_medis':prefilled_data['no_rekam_medis']})
        form.fields['id_transaksi'].disabled = True
        form.fields['total_biaya'].disabled = True
        form.fields['no_rekam_medis'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        id_transaksi = form.cleaned_data['id_transaksi']
        tanggal = form.cleaned_data['tanggal']
        status = form.cleaned_data['status']
        total_biaya = form.cleaned_data['total_biaya']
        waktu_pembayaran = form.cleaned_data['waktu_pembayaran']
        no_rekam_medis = form.cleaned_data['no_rekam_medis']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update transaksi set id_transaksi=%s, tanggal=%s, status=%s, total_biaya=%s, waktu_pembayaran=%s, no_rekam_medis=%s where id_transaksi=%s",[id_transaksi,tanggal,status,total_biaya,waktu_pembayaran,no_rekam_medis,id_transaksi])
        return redirect("/transaksi/display")

    return render(request,'update-transaksi.html',response)
	
def get_transaksi_with_id(id):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from transaksi where id_transaksi=%s",[id])
    res_dirty = fetch(cursor)
    return res_dirty[0]