from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='transaksi'

urlpatterns = [
    path('create/',views.create_transaksi,name='create-transaksi'),
    path('create-success/',views.create_success,name='create-success'),
    path('display/',views.display_transaksi,name='display-transaksi'),
	path('delete/',views.delete_transaksi,name='delete-transaksi'),
	path('update/',views.update_transaksi,name='update-transaksi'),
]