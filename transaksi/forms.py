from django import forms

class CreateTransaksiForm(forms.Form):
    no_rekam_medis=forms.ChoiceField(choices=[])
	
class UpdateTransaksiForm(forms.Form):
    id_transaksi=forms.CharField(max_length=50)
    tanggal=forms.DateField()
    status=forms.CharField(max_length=50)
    total_biaya=forms.IntegerField()
    waktu_pembayaran=forms.DateTimeField()
    no_rekam_medis=forms.CharField(max_length=50)