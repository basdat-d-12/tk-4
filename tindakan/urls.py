from django.conf.urls import url
from django.urls import include,path
from tindakan import views

app_name='tindakan'

urlpatterns = [
    path('',views.tindakan,name='tindakan'),
    path('create/',views.create_tindakan,name='create-tindakan'),
    path('display/',views.display_tindakan,name='display-tindakan'),
    path('delete/',views.delete_tindakan,name='delete-tindakan'),
    path('update/',views.update_tindakan,name='update-tindakan'),
]