from django import forms

class CreateTindakanForm(forms.Form):
    id_konsultasi=forms.ChoiceField(label='ID Konsultasi',choices=[])
    id_transaksi=forms.ChoiceField(label='ID transaksi',choices=[])
    catatan=forms.CharField(label='Catatan', required=False)
    id_tindakan_poliklinik=forms.ChoiceField(label='ID tindakan poliklinik',choices=[])

class UpdateTindakanForm(forms.Form):
    id_konsultasi=forms.CharField(label='ID Konsultasi',max_length=50,required=False)
    no_urut=forms.CharField(label='Nomor urut',max_length=50,required=False)
    biaya=forms.IntegerField(label='Biaya',required=False)
    catatan=forms.CharField(label='Catatan', required=False)
    id_transaksi=forms.CharField(label='ID transaksi',max_length=50,required=False)

