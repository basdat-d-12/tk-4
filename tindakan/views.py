from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import CreateTindakanForm, UpdateTindakanForm
from django.views.decorators.csrf import csrf_exempt
import time
import json

def tindakan(request):
    return redirect('/tindakan/display')

def create_tindakan(request):
    form = CreateTindakanForm(request.POST or None)
    if "is_admin" not in request.session:
        return render(request,'unauthorized.html')

    if(request.method=="POST"):
        rp = request.POST
        arr_id_tindakan_poli = rp.getlist('id_tindakan_poliklinik')
        arr_id_tindakan_poli.sort()
        for i in range(0, len(arr_id_tindakan_poli)):
            for j in range (i+1, len(arr_id_tindakan_poli)):
                if arr_id_tindakan_poli[i]==arr_id_tindakan_poli[j]:
                    return redirect('/tindakan/create')
        id_konsultasi = rp.get('id_konsultasi')
        id_transaksi = rp.get('id_transaksi')
        catatan = rp.get('catatan')
        no_urut = get_new_no_urut()

        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into tindakan (id_konsultasi,no_urut,biaya,catatan,id_transaksi) values(%s,%s,%s,%s,%s);",[id_konsultasi,no_urut,0,catatan,id_transaksi])
        
        for id_tindakan_poli in arr_id_tindakan_poli:
            create_daftar_tindakan(id_konsultasi,no_urut,id_tindakan_poli)

        return redirect('/tindakan/display')

    response = {}
    response['error'] = False
    id_konsultasi = get_id_konsultasi()
    id_transaksi = get_id_transaksi()
    id_tindakan_poliklinik = get_id_tindakan_poliklinik()
    form.fields['id_konsultasi'].choices = id_konsultasi
    form.fields['id_transaksi'].choices = id_transaksi
    form.fields['id_tindakan_poliklinik'].choices=id_tindakan_poliklinik
    response['form'] = form
    return render(request,'create-tindakan.html',response)

def create_daftar_tindakan(id_konsultasi, no_urut, id_tindakan_poli):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("insert into daftar_tindakan (id_konsultasi,no_urut,id_tindakan_poli) values(%s,%s,%s)",[id_konsultasi,no_urut,id_tindakan_poli])

def display_tindakan(request):
    if "username" not in request.session:
        return redirect('/login')
    tindakan_raw = get_tindakan()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = tindakan_raw
    return render(request,'display-tindakan.html',response)

def get_tindakan():
    query="""
    SELECT t.id_konsultasi, t.no_urut, t.biaya, t.catatan, t.id_transaksi, STRING_AGG(dt.id_tindakan_poli, ', ')
    FROM tindakan t NATURAL JOIN daftar_tindakan dt
    GROUP BY t.no_urut, t.id_konsultasi ORDER BY t.id_konsultasi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    tindakan_response = fetch(cursor)
    return tindakan_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def get_id_konsultasi():
    query="""
    select id_konsultasi from sesi_konsultasi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_konsultasi_response = fetch(cursor)
    res=[]
    for id_konsultasi in id_konsultasi_response:
        temp = id_konsultasi['id_konsultasi']
        res.append((temp,temp,))
    return res

def get_id_transaksi():
    query="""
    select id_transaksi from transaksi;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_transaksi_response = fetch(cursor)
    res=[]
    for id_transaksi in id_transaksi_response:
        temp = id_transaksi['id_transaksi']
        res.append((temp,temp,))
    return res

def get_id_tindakan_poliklinik():
    query="""
    select id_tindakan_poli from tindakan_poli order by id_tindakan_poli;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_tindakan_poliklinik_response = fetch(cursor)
    res=[]
    for id_tindakan_poliklinik in id_tindakan_poliklinik_response:
        temp = id_tindakan_poliklinik['id_tindakan_poli']
        res.append((temp,temp,))
    return res

def get_new_no_urut():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select no_urut from tindakan;")
    no_urut_response = fetch(cursor)
    current_no_urut = []
    for no_urut in no_urut_response:
        current_no_urut.append(int(no_urut['no_urut']))
    if (len(current_no_urut)==0):
        return 1
    return max(current_no_urut) + 1

@csrf_exempt
def delete_tindakan(request):
    if request.method == "POST":
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_konsultasi = body['payload']['id_konsultasi']
        no_urut = body['payload']['no_urut']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from tindakan where id_konsultasi=%s and no_urut=%s;",[id_konsultasi,no_urut])
        return redirect("/tindakan/display")
    return HttpResponse("Nothing here")    

@csrf_exempt
def update_tindakan(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')

    id_konsultasi = request.GET.get('id_konsultasi')
    no_urut = request.GET.get('no_urut')
    if(id_konsultasi != None and no_urut != None):
        prefilled_data = get_specific_tindakan(id_konsultasi, no_urut)
        response={}
        response['error']=False
        form = UpdateTindakanForm(request.POST or None, 
            initial={'id_konsultasi':id_konsultasi,'no_urut':no_urut,'biaya':prefilled_data['biaya'],
            'catatan':prefilled_data['catatan'],'id_transaksi':prefilled_data['id_transaksi']})
        daftar_id_tindakan_poli = get_daftar_id_tindakan_poli(id_konsultasi, no_urut)
        form.fields['id_konsultasi'].disabled = True
        form.fields['no_urut'].disabled = True
        form.fields['biaya'].disabled = True
        form.fields['id_transaksi'].disabled = True
        response['form'] = form
        response['data'] = daftar_id_tindakan_poli

    if(request.method == "POST" and form.is_valid()):
        catatan = request.POST.get('catatan')
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update tindakan set catatan=%s where id_konsultasi=%s and no_urut=%s;"
                        ,[catatan,id_konsultasi,no_urut])
        return redirect("/tindakan/display")
    return render(request,'update-tindakan.html',response)

def get_specific_tindakan(id_konsultasi,no_urut):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from tindakan where id_konsultasi=%s and no_urut=%s",[id_konsultasi,no_urut])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_daftar_id_tindakan_poli(id_konsultasi,no_urut):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_tindakan_poli from daftar_tindakan where id_konsultasi=%s and no_urut=%s",[id_konsultasi,no_urut])
    res_dirty = fetch(cursor)
    return res_dirty