from django.shortcuts import render,redirect
from django.db import connection

def home(request):
    context={}
    if "username" in request.session:
        context["is_logged_in"]=True
        return redirect('/profile')
    else:
        context["is_logged_in"] = False
    return render(request,'pages/index.html',context)

def profile(request):
    if "username" not in request.session:
        return redirect('/')
    context={}
    context['is_admin'] = "is_admin" in request.session
    context['username'] = request.session['username']
    context['data_pasien'] = get_pasien()
    context['data_dokter'] = get_dokter()
    context['user_data'] = get_current_user_data(request.session['username'])
    context['data_administrator'] = get_administrator()
    return render(request,'pages/profile.html',context)

def get_current_user_data(username):
    query="""
    SELECT * FROM pengguna
    WHERE username ='{}'""".format(username)
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    pengguna_response = fetch(cursor)
    return pengguna_response

def get_administrator():
    query="""
    SELECT * FROM administrator NATURAL JOIN pengguna;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    administrator_response = fetch(cursor)
    return administrator_response

def get_dokter():
    query="""
    SELECT * FROM dokter NATURAL JOIN pengguna;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    dokter_response = fetch(cursor)
    return dokter_response

def get_pasien():
    query="""
    SELECT * FROM pasien NATURAL JOIN pengguna;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    pasien_response = fetch(cursor)
    return pasien_response          

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

