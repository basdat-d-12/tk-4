from django.conf.urls import url
from django.urls import include,path
from tindakan_poliklinik import views

app_name='tindakan_poliklinik'

urlpatterns = [
    path('',views.tindakan_poliklinik,name='tindakan-poliklinik'),
    path('create/',views.create_tindakan_poliklinik,name='create-tindakan-poliklinik'),
    path('display/',views.display_tindakan_poliklinik,name='display-tindakan-poliklinik'),
    path('delete/',views.delete_tindakan_poliklinik,name='delete-tindakan-poliklinik'),
    path('update/',views.update_tindakan_poliklinik,name='update-tindakan-poliklinik')
]