from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import CreateTindakanPoliklinikForm,UpdateTindakanPoliklinikForm
import json
from django.views.decorators.csrf import csrf_exempt

def tindakan_poliklinik(request):
    return redirect('/tindakan-poliklinik/display')

def create_tindakan_poliklinik(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if "username" not in request.session:
        return redirect('/login')

    response = {}
    response['error'] = False
    form = CreateTindakanPoliklinikForm(request.POST or None)
    form.fields['id_poliklinik'].choices = get_id_poliklinik()
    response['form'] = form

    if(request.method=="POST" and form.is_valid()):
        id_poliklinik = form.cleaned_data['id_poliklinik']
        nama_tindakan = form.cleaned_data['nama_tindakan']
        deskripsi = form.cleaned_data['deskripsi']
        tarif = form.cleaned_data['tarif']
        id_tindakan_poli = get_new_id_tindakan_poli()
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into tindakan_poli (id_tindakan_poli,id_poliklinik,nama_tindakan,tarif,deskripsi) values(%s,%s,%s,%s,%s);"
                        ,[id_tindakan_poli,id_poliklinik,nama_tindakan,tarif,deskripsi])
                        
        return redirect('/tindakan-poliklinik/display')
    return render(request,'create-tindakan-poliklinik.html',response)

def get_new_id_tindakan_poli():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_tindakan_poli from tindakan_poli;")
    response = fetch(cursor)
    arr_id_tindakan_poli = []
    for id_tindakan_poli in response:
        arr_id_tindakan_poli.append(int(id_tindakan_poli['id_tindakan_poli']))
    if (len(arr_id_tindakan_poli)==0):
        return 1
    return int(max(arr_id_tindakan_poli)) + 1

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

def display_tindakan_poliklinik(request):
    if "username" not in request.session:
        return redirect('/login')
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = get_tindakan_poliklinik()
    return render(request,'display-tindakan-poliklinik.html',response)

def get_tindakan_poliklinik():
    query="""
    SELECT tp.id_tindakan_poli, tp.id_poliklinik, tp.nama_tindakan, tp.deskripsi, tp.tarif
    FROM tindakan_poli tp ORDER BY tp.id_tindakan_poli;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    tindakan_poliklinik_response = fetch(cursor)
    return tindakan_poliklinik_response

def get_id_poliklinik():
    query="""
    select id_poliklinik from layanan_poliklinik;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    id_poliklinik_response = fetch(cursor)
    res=[]
    for id_poliklinik in id_poliklinik_response:
        temp = id_poliklinik['id_poliklinik']
        res.append((temp,temp,))
    return res

@csrf_exempt
def delete_tindakan_poliklinik(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_tindakan_poli = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from tindakan_poli where id_tindakan_poli =%s;",[id_tindakan_poli])
        return redirect("/tindakan-poliklinik/display")
    return HttpResponse("Nothing here")

@csrf_exempt
def update_tindakan_poliklinik(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    id_tindakan_poli = request.GET.get('id_tindakan_poli',None)
    if(id_tindakan_poli != None):
        prefilled_data = get_tindakan_poli(id_tindakan_poli)
        response={}
        response['error']=False
        form = UpdateTindakanPoliklinikForm(request.POST or None, 
            initial={'id_tindakan_poli':id_tindakan_poli,'nama_tindakan':prefilled_data['nama_tindakan'],
            'deskripsi':prefilled_data['deskripsi'],'tarif':prefilled_data['tarif']})
        form.fields['id_poliklinik'].choices = get_id_poliklinik()
        form.fields['id_tindakan_poli'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        id_tindakan_poli = form.cleaned_data['id_tindakan_poli']
        id_poliklinik = form.cleaned_data['id_poliklinik']
        nama_tindakan = form.cleaned_data['nama_tindakan']
        deskripsi = form.cleaned_data['deskripsi']
        tarif = form.cleaned_data['tarif']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update tindakan_poli set id_poliklinik=%s,nama_tindakan=%s,tarif=%s,deskripsi=%s where id_tindakan_poli=%s;"
                        ,[id_poliklinik,nama_tindakan,tarif,deskripsi,id_tindakan_poli])
        return redirect("/tindakan-poliklinik/display")

    return render(request,'update-tindakan-poliklinik.html',response)

def get_tindakan_poli(id_tindakan_poli):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from tindakan_poli where id_tindakan_poli=%s",[id_tindakan_poli])
    res_dirty = fetch(cursor)
    return res_dirty[0]