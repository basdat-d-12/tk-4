from django.conf.urls import url
from django.urls import include,path
from . import views

app_name='obat'

urlpatterns=[
    path('create/',views.create_obat,name='create-obat'),
    path('display/',views.display_obat,name='display-obat'),
    path('delete-obat/',views.delete_obat,name='delete-obat'),
    path('update/',views.update_obat,name='update-obat'),
]