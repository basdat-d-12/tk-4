from django.shortcuts import render, redirect, HttpResponse
from django.db import connection
from .forms import CreateObatForm,UpdateObatForm
import json
from django.views.decorators.csrf import csrf_exempt

def create_obat(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if "username" not in request.session:
        return redirect('/login')

    response={}
    response['error']=False
    form = CreateObatForm(request.POST or None)
    response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        print(form.cleaned_data)
        kode = form.cleaned_data['kode']
        stok = form.cleaned_data['stok']
        harga = form.cleaned_data['harga']
        komposisi = form.cleaned_data['komposisi']
        bentuk_sediaan = form.cleaned_data['bentuk_sediaan']
        merk_dagang = form.cleaned_data['merk_dagang']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into obat (kode,stok,harga,komposisi,bentuk_sediaan,merk_dagang) values(%s,%s,%s,%s,%s,%s);",[kode,stok,harga,komposisi,bentuk_sediaan,merk_dagang])
        return redirect('/obat/display')
    return render(request,'create-obat.html',response)

def display_obat(request):
    if "username" not in request.session:
        return redirect('/login')
    response={}
    response['data'] = get_obat()
    response['is_admin'] = "is_admin" in request.session
    return render(request,'display-obat.html',response)

def get_obat():
    query = """
    select * from obat;
    """
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute(query)
    obat_response = fetch(cursor)
    return obat_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_obat(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        kode_obat = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from obat where kode =%s;",[kode_obat])
        return redirect("/obat/display")
    return HttpResponse("Nothing here")

@csrf_exempt
def update_obat(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    kode_obat = request.GET.get('kode',None)
    if(kode_obat != None):
        prefilled_data = get_obat_with_kode(kode_obat)
        response={}
        response['error']=False
        form = UpdateObatForm(request.POST or None, initial={'kode':kode_obat,'stok':prefilled_data['stok'],'harga':prefilled_data['harga'],'komposisi':prefilled_data['komposisi'],'bentuk_sediaan':prefilled_data['bentuk_sediaan'],'merk_dagang':prefilled_data['merk_dagang']})
        form.fields['kode'].disabled = True
        response['form'] = form

    if(request.method == "POST" and form.is_valid()):
        kode = form.cleaned_data['kode']
        stok = form.cleaned_data['stok']
        harga = form.cleaned_data['harga']
        komposisi = form.cleaned_data['komposisi']
        bentuk_sediaan = form.cleaned_data['bentuk_sediaan']
        merk_dagang = form.cleaned_data['merk_dagang']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update obat set kode=%s, stok=%s, harga=%s,komposisi=%s,bentuk_sediaan=%s,merk_dagang=%s where kode=%s",[kode,stok,harga,komposisi,bentuk_sediaan,merk_dagang,kode])
        return redirect("/obat/display")
    return render(request,'update-obat.html',response)

def get_obat_with_kode(kode):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from obat where kode=%s",[kode])
    res_dirty = fetch(cursor)
    return res_dirty[0]