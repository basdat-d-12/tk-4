from django import forms

class CreateObatForm(forms.Form):
    kode=forms.CharField(label='Kode',max_length=50)
    stok=forms.IntegerField(label='Stok')
    harga=forms.IntegerField(label='Harga')
    komposisi=forms.CharField(label='Komposisi',required=False)
    bentuk_sediaan=forms.CharField(label='Bentuk sediaan',max_length=50)
    merk_dagang=forms.CharField(label='Merk Dagang',max_length=50)

class UpdateObatForm(forms.Form):
    kode=forms.CharField(label='Kode',max_length=50,required=False)
    stok=forms.IntegerField(label='Stok')
    harga=forms.IntegerField(label='Harga')
    komposisi=forms.CharField(label='Komposisi',required=False)
    bentuk_sediaan=forms.CharField(label='Bentuk sediaan',max_length=50)
    merk_dagang=forms.CharField(label='Merk Dagang',max_length=50)