from django import forms

class CreateLayananPoliklinikForm(forms.Form):
    nama_layanan=forms.CharField(label='Nama Layanan', required=True)
    deskripsi=forms.CharField(label='Deskripsi', required=False)
    kode_rs_cabang=forms.ChoiceField(choices=[])  
    
class CreateJadwalPoliklinikForm(forms.Form):    
    hari=forms.CharField(required=True)
    waktu_mulai=forms.TimeField(required=True)
    waktu_selesai=forms.TimeField(required=True)
    kapasitas=forms.IntegerField(required=True)

class UpdateLayananPoliklinikForm(forms.Form):
    id_poliklinik=forms.CharField(label='ID Poliklinik', required=True)
    nama_layanan=forms.CharField(label='Nama Layanan', required=True)
    deskripsi=forms.CharField(label='Deskripsi', required=False)
    kode_rs_cabang=forms.ChoiceField(choices=[])  

class UpdateJadwalPoliklinikForm(forms.Form):    
    id_jadwal_poliklinik=forms.CharField(label='ID Jadwal Poliklinik', required=True)
    hari=forms.CharField(required=True)
    waktu_mulai=forms.TimeField(required=True)
    waktu_selesai=forms.TimeField(required=True)
    kapasitas=forms.IntegerField(required=True)
    id_dokter=forms.ChoiceField(choices=[])  
    id_poliklinik=forms.CharField(label='ID Poliklinik', required=True)