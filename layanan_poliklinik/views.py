from django.shortcuts import render, redirect
from django.db import connection
from .forms import CreateLayananPoliklinikForm, CreateJadwalPoliklinikForm, UpdateLayananPoliklinikForm, UpdateJadwalPoliklinikForm
import json
from django.views.decorators.csrf import csrf_exempt
import time
import random

def create_layanan_poliklinik(request):
    # kalo dia bukan admin, tampilin unauthorized
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    if "username" not in request.session:
        return redirect('/login')
    response = {}
    response['error'] = False    
    kode_rs_cabang = get_kode_rs_cabang()
    form = CreateLayananPoliklinikForm(request.POST or None)
    forms = CreateJadwalPoliklinikForm(request.POST or None, initial={'hari':"Senin",'waktu_mulai':'12:00:00','waktu_selesai':'14:00:00','kapasitas':"10"})
    form.fields['kode_rs_cabang'].choices = kode_rs_cabang
    response['form'] = form
    response['forms'] = forms
    if(request.method=="POST" and form.is_valid() and forms.is_valid()):
        print(form.cleaned_data)
        print(forms.cleaned_data)
        nama_layanan = form.cleaned_data['nama_layanan']
        deskripsi = form.cleaned_data['deskripsi']
        kode_rs_cabang = form.cleaned_data['kode_rs_cabang']
        id_poliklinik = get_id_poliklinik()
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("insert into layanan_poliklinik (id_poliklinik,kode_rs_cabang,nama,deskripsi) values(%s,%s,%s,%s);",[id_poliklinik,kode_rs_cabang,nama_layanan,deskripsi])
        n=random.randint(0,6)
        gid = get_id_dokter()
        id_dokter = gid[n][1]
        rp = request.POST
        arr_hari = rp.getlist('hari')
        arr_waktu_mulai = rp.getlist('waktu_mulai')
        arr_waktu_selesai = rp.getlist('waktu_selesai')
        arr_kapasitas = rp.getlist('kapasitas')
        jumlah_jadwal = len(arr_hari)
        for i in range(jumlah_jadwal):
            id_jadwal_poliklinik = get_id_jadwal_poliklinik(id_poliklinik)
            cursor.execute("insert into jadwal_layanan_poliklinik (id_jadwal_poliklinik,waktu_mulai,waktu_selesai,hari,kapasitas,id_dokter,id_poliklinik) values(%s,%s,%s,%s,%s,%s,%s);",[id_jadwal_poliklinik,arr_waktu_mulai[i],arr_waktu_selesai[i],arr_hari[i],arr_kapasitas[i],id_dokter,id_poliklinik])
        return redirect('/sesikonsultasi/display')
    return render(request,'create-layanan_poliklinik.html',response)
 
def get_id_poliklinik():
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select MAX(id_poliklinik) from layanan_poliklinik;")
    id_poliklinik_response = fetch(cursor)
    for id_pol in id_poliklinik_response:
        temp = id_pol['max']
    if(temp==None):
        return 1
    return int(temp) + 1

def get_id_jadwal_poliklinik(id_poliklinik):
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select MAX(id_jadwal_poliklinik) from jadwal_layanan_poliklinik where id_poliklinik='%s';",[id_poliklinik])
    id_jadwal_poliklinik_response = fetch(cursor)
    for id_pol in id_jadwal_poliklinik_response:
        temp = id_pol['max']
    if(temp==None):
        return 1
    return int(temp) + 1

def get_kode_rs_cabang():
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select kode_rs from rs_cabang;")
    kode_rs_cabang_response = fetch(cursor)
    res=[]
    for kode_rs_cabang in kode_rs_cabang_response:
        temp = kode_rs_cabang['kode_rs']
        res.append((temp,temp,))
    return res

def display_layanan_poliklinik(request):
    if "username" not in request.session:
        return redirect('/login')
    layanan_poliklinik_raw = get_layanan_poliklinik()
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = layanan_poliklinik_raw
    return render(request,'display-layanan_poliklinik.html',response)

def get_layanan_poliklinik():
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from layanan_poliklinik order by id_poliklinik ASC;")
    id_poliklinik_response = fetch(cursor)
    return id_poliklinik_response

def display_jadwal_poliklinik(request):
    if "username" not in request.session:
        return redirect('/login')
    id_poli = request.GET.get('id_poliklinik',None)
    layanan_poliklinik_raw = get_jadwal_poliklinik(id_poli)
    response = {}
    response['is_admin'] = "is_admin" in request.session
    response['data'] = layanan_poliklinik_raw
    return render(request,'display-jadwal_poliklinik.html',response)

def get_jadwal_poliklinik(id_poli):
    cursor=connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from jadwal_layanan_poliklinik where id_poliklinik=%s",[id_poli])
    id_poliklinik_response = fetch(cursor)
    return id_poliklinik_response

def fetch(cursor):
    columns = [col[0] for col in cursor.description]
    return [dict(zip(columns, row)) for row in cursor.fetchall()]

@csrf_exempt
def delete_layanan_poliklinik(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_layanan = body['payload']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from layanan_poliklinik where id_poliklinik =%s;",[id_layanan])
        return redirect("/layanan_poliklinik/display")
    return HttpResponse("Nothing here")

@csrf_exempt
def delete_jadwal_poliklinik(request):
    if(request.method == "POST"):
        body_unicode = request.body.decode('utf-8')
        body = json.loads(body_unicode)
        id_jadwal = body['payload']['id_jadwal']
        id_poli = body['payload']['id_poli']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("delete from jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s and id_poliklinik=%s;",[id_jadwal,id_poli])
        return redirect("/layanan_poliklinik/display-jadwal")
    return HttpResponse("Nothing here")

@csrf_exempt
def update_layanan_poliklinik(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    id_poli = request.GET.get('id_poliklinik',None)
    if(id_poli != None):
        prefilled_data = get_layanan_poliklinik_with_id(id_poli)
        response={}
        response['error']=False
        form = UpdateLayananPoliklinikForm(request.POST or None, initial={'id_poliklinik':id_poli,'nama_layanan':prefilled_data['nama'],'deskripsi':prefilled_data['deskripsi'],'kode_rs_cabang':prefilled_data['kode_rs_cabang']})
        kode_rs_cabang = get_kode_rs_cabang()
        form.fields['kode_rs_cabang'].choices = kode_rs_cabang
        form.fields['id_poliklinik'].disabled = True
        response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        id_poliklinik = form.cleaned_data['id_poliklinik']
        nama_layanan = form.cleaned_data['nama_layanan']
        deskripsi = form.cleaned_data['deskripsi']
        kode_rs_cabang = form.cleaned_data['kode_rs_cabang']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update layanan_poliklinik set id_poliklinik=%s, nama=%s, deskripsi=%s,kode_rs_cabang=%s where id_poliklinik=%s",[id_poli,nama_layanan,deskripsi,kode_rs_cabang,id_poliklinik])
        return redirect("/layanan_poliklinik/display")
    return render(request,'update-layanan.html',response)

def get_layanan_poliklinik_with_id(id_poli):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from layanan_poliklinik where id_poliklinik=%s",[id_poli])
    res_dirty = fetch(cursor)
    return res_dirty[0]

@csrf_exempt
def update_jadwal_poliklinik(request):
    if "is_admin" not in request.session:
        return redirect('/unauthorized')
    id_jadwal = request.GET.get('id_jadwal_poliklinik',None)
    id_poli = request.GET.get('id_poliklinik',None)
    if(id_jadwal != None):
        prefilled_data = get_jadwal_poliklinik_with_id(id_jadwal,id_poli)
        response={}
        response['error']=False
        form = UpdateJadwalPoliklinikForm(request.POST or None, initial={'id_jadwal_poliklinik':id_jadwal,'hari':prefilled_data['hari'],'waktu_mulai':prefilled_data['waktu_mulai'],'waktu_selesai':prefilled_data['waktu_selesai'],'kapasitas':prefilled_data['kapasitas'],'id_dokter':prefilled_data['id_dokter'],'id_poliklinik':id_poli})
        id_dokter = get_id_dokter()
        form.fields['id_jadwal_poliklinik'].disabled = True
        form.fields['id_dokter'].choices = id_dokter
        form.fields['id_poliklinik'].disabled = True
        response['form'] = form
    if(request.method == "POST" and form.is_valid()):
        hari = form.cleaned_data['hari']
        waktu_mulai = form.cleaned_data['waktu_mulai']
        waktu_selesai = form.cleaned_data['waktu_selesai']
        id_dokter = form.cleaned_data['id_dokter']
        kapasitas = form.cleaned_data['kapasitas']
        cursor = connection.cursor()
        cursor.execute("set search_path to medika_go;")
        cursor.execute("update jadwal_layanan_poliklinik set id_jadwal_poliklinik=%s, hari=%s, waktu_mulai=%s,waktu_selesai=%s,id_dokter=%s,kapasitas=%s where id_poliklinik=%s and id_jadwal_poliklinik=%s",[id_jadwal,hari,waktu_mulai,waktu_selesai,id_dokter,kapasitas,id_poli,id_jadwal])
        back_to_prev = "/layanan_poliklinik/display-jadwal/?id_poliklinik="+id_poli
        return redirect(back_to_prev)
    return render(request,'update-jadwal.html',response)

def get_jadwal_poliklinik_with_id(id_jadwal,id_poli):
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select * from jadwal_layanan_poliklinik where id_jadwal_poliklinik=%s and id_poliklinik=%s",[id_jadwal,id_poli])
    res_dirty = fetch(cursor)
    return res_dirty[0]

def get_id_dokter():
    cursor = connection.cursor()
    cursor.execute("set search_path to medika_go;")
    cursor.execute("select id_dokter from dokter;")
    id_dokter_admin_response = fetch(cursor)
    id_dokter_admin = []
    for id_dokter in id_dokter_admin_response:
        idd = id_dokter['id_dokter']
        res = (idd,idd,)
        id_dokter_admin.append(res)
    return id_dokter_admin